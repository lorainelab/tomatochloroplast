# Visualizing the primers used by BINF 6350 students
The students used long range PCR to amplify regions of the chloroplast genome for DNA sequencing.  The amplicons of separate PCR reactions should overlap. Some primer pairs may have multiple products, and some products may have less effeciency due to excess primer mis-alignment.  Visualizing the primers and the amplicons alongside the sequencing data may help us understand patterns we see in the data.

* * * 

## Primers2Amplicons.Rmd

Uses the primer sequences from data/Primers.xlsx and AlignPrimers.sh to determine where primers align and what amplicons are made. Creates a bed file showing primer alignments and a bed file showing amplicons.

* * * 

## AlignPrimers.sh

Shell script that calls [blat](http://genome.ucsc.edu/goldenpath/help/blatSpec.html) to align primers to the chloroplast sequence.



