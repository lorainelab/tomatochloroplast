#!/bin/sh

# addBlatSuite
PATH=$PATH:/usr/local/bin/BlatSuite:/usr/local/bin/BlatSuite/blat
# addSamtools
PATH=$PATH:/usr/local/bin/samtools

errLog=intermediateFiles/PrimerAlignmentSummary.txt

# run blat
blat -out=pslx -t=dna -q=dna -minScore=0 -tileSize=6 ../Sequence/results/chrC-NC_007898.3.fasta intermediateFiles/primers.fa results/alignedPrimers.pslx 2> $errLog
#note that -minScore is set to a value smaller than the length of the primers.

#grep chr results/alignedPrimers.psl | cut -f 10,12,13,14 > results/alignedPrimers.bed

# psl2sam.pl results/alignedPrimers.psl > results/primers.sam

# samtools view -b -S results/primers.sam > primers.temp.bam 2> $errLog
#rm results/primers.sam

# samtools sort -o results/primers.bam -O bam -T primers.2.temp primers.temp.bam 2> $errLog
# rm primers.temp.bam
# samtools index -b results/primers.bam

# samtools --version >> $errLog

