# Tomato chloroplast - data

## What is this folder
In the module structure that the Loraine Lab uses for most projects, the "data" folder is a place to store the files used as input in a process.

The inputs for GenBankToBedDetail.Rmd are the sequence file and the sequence.gb which includes the annotation data.

* * *

### Sequence data
The sequence is downloaded automatically in the GenBankToBedDetail markdown using a package called "ape".

* * *

### Annotation data
sequence.gb was downloaded from NCBI, http://www.ncbi.nlm.nih.gov/nuccore/NC_007898.3  

By selecting "send to" --> "complete record" --> "file" --> "GenBank"

Line 1174 was broken in an unexpected way. I edited this in the .gb file so the line reads:
"     CDS             join(complement(71636..71749),141306..141537,142074..142099)"

* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu
* Ann Loraine aloraine@uncc.edu
