# Tomato chloroplast annotations


* * * 

## data

This folder holds the data downloaded from NCBI ([Solanum lycopersicum chloroplast, complete genome](http://www.ncbi.nlm.nih.gov/nuccore/NC_007898.3)) that are used as the input for the GenBankToBedDetail markdown. 

* * * 

## GenBankToBedDetail.Rmd

This markdown documents methods used to generate the tomato chloroplast annotation files for IGB. 

* * * 

## results

This folder holds the output of the GenBankToBedDetail markdown. These files are ready to be viewed in IGB.

* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu (who did the work) 
* Ann Loraine aloraine@uncc.edu (reviewer, Principal Investigator)
