# Tomato chloroplast - results

## What is this folder
In the module structure that the Loraine Lab uses for most projects, the "results" folder is a place to store the output files of a process.

* * *

### Solanum_lycpersycum_chloroplast_2013.bed.gz
The annotation information from GenBank in bed detail format, zipped with bgzip.

* * *

### Solanum_lycpersycum_chloroplast_2013.bed.gz.tbi
A tabix index to accompany the zipped bed detail file.

* * *

# View in IGB
You can view these files in IGB before they are added to IGBquickload.org.

 * Use the sequence (the .fasta file) as a custom genome in IGB. Go to File, and select "open genome from file" and navigate to the fasta file.
 * To view the annotations (the .bed.gz file) drag the file from a finder window onto the IGB window or navigate to it by going through the IGB File menu and selecting "open file".
 
The tabix file just needs to exist in the same location as the .bed.gz file.

* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu
* Ann Loraine aloraine@uncc.edu
