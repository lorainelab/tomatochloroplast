<h1>BINF 6350 Student Data</h1>
<p><b>University of North Carolina at Charlotte<br>Department of Bioinformatics and Genomics</b></p>
<p>Students taking BINF 6350 generated data under the supervision of Dr. Jennifer Weller and Dr. Cathy Moore.  Students prepared the data for submission under the supervision of Dr. Cynthia Gibas.</p>
<p>The data are available in fastq format from the NCBI Sequence Read Archive under two project accessions:
<ul>
<li><a href="http://www.ncbi.nlm.nih.gov/bioproject/PRJNA256298">SRP044909</a>
<li><a href="http://www.ncbi.nlm.nih.gov/bioproject/PRJNA256329">SRP052069</a>
</ul>
</p>
<p>The data were aligned to the <a href="http://www.ncbi.nlm.nih.gov/nuccore/NC_007898.3">tomato chloroplast</a> using <a href="http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml">bowtie2</a> version 2.1.0 with the --local setting.</p>
<h3>Extraction and Sequencing Methods</h3>
<p>A sample of fresh-frozen leaf tissue was harvested separately from 15 tomato cultivars. After chloroform extraction, RNaseA and Proteinase K digestion, incubation in high salt, and a second phenol-chloroform extraction the material was precipitated with isopropanol and quantified using a Nanodrop 2000. The chloroplast genome was then amplified using overlapping primers that give ~15,000 bp amplicons. Primers and conditions available upon request, and will be published. We ligated bar-coded adapters using the Ion Xpress kit. The library was quantified using the Kapa Biosystems kit for the Ion Torrent and BioRad iCycler. After appropriate dilutions, emPCR was performed using the Ion 400bp kit reagents and conditions for the Ion OneTouch2 instrument. Confirmation of ISP loading with the Qubit was followed by sequencing of the libraries on the Ion Torrent PGM, using the 400bp sequencing kit. Data was generated at the University of North Carolina at Charlotte during the Fall 2013 term.</p>

<h3>Sample Accessions</h3>
Baxter's Early Bush_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531281">(SRR1531281)</a>
<br>Baxter's Early Bush_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763774">(SRR1763774)</a>
<br>Black Cherry_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531284">(SRR1531284)</a>
<br>Black cherry_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763775">(SRR1763775)</a>
<br>Cherokee Purple_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531285">(SRR1531285)</a>
<br>Cherokee Purple_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763776">(SRR1763776)</a>
<br>Cherry_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1528539">(SRR1528539)</a>
<br>Cherry_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763770">(SRR1763770)</a>
<br>Delicious_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531286">(SRR1531286)</a>
<br>Delicious_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763777">(SRR1763777)</a>
<br>German Cherry_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531282">(SRR1531282)</a>
<br>German Cherry_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763780">(SRR1763780)</a>
<br>Hardin's Miniature_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763773">(SRR1763773)</a>
<br>Hardin's Miniature_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531280">(SRR1531280)</a>
<br>Heinz 1706-BG_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531283">(SRR1531283)</a>
<br>Jersey Devil_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531287">(SRR1531287)</a>
<br>Jersey Devil_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763778">(SRR1763778)</a>
<br>LA2377_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763772">(SRR1763772)</a>
<br>Mortgage Lifter_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531289">(SRR1531289)</a>
<br>Mortgage Lifter_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763781">(SRR1763781)</a>
<br>Purple Calabash_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1528540">(SRR1528540)</a>
<br>Purple Calabash_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763771">(SRR1763771)</a>
<br>Yellow Stuffer_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1531288">(SRR1531288)</a>
<br>Yellow Stuffer_b <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1763779">(SRR1763779)</a>
<br>Yellow Tomato_a <a href="http://www.ncbi.nlm.nih.gov/sra/SRR1528541">(SRR1528541)</a>