# Quickload files for BINF 6350 student data


* * * 

## MakeAnnots_usingPreAnnots.Rmd

Uses the info in preAnnotes.xlsx to make annots.xml and ExtractedAnnotes.xml

* * * 

## quickload_Header

Folder containing the HEADER.md file for the BINF6350 folder

* * * 

## ColorPicker.pptx

Power point slide to help choose and manage colors to use in quickload.


