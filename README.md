# Tomato chloroplast sequence and annotations

This repository documents acquiring and processing tomato chloroplast sequence and annotation information for display in [Integrated Genome Browser](http://www.bioviz.org).

The nuclear genome and annotation were from SolGenomics and are already available in IGB.  

The mitochondrial sequence is currently only available as a series of scaffolds, see [S_lycopersicum_mitochondrion_v1.5](http://www.ncbi.nlm.nih.gov/assembly/512708/).

* * * 

## Annotation

Module to convert the annotations available from GenBank into the bed detail format that can be viewed in IGB. 

* * * 

## BINF6350_StudentData

Module to create the meta data files to make up the quickload site to house the student-generated tomato chloroplast genome re-sequncing data.

* * * 

## Sequence

Module containing the code to download and save the tomato chloroplast sequence.

* * *

## src

Any functions or variables used in more than one module is stored here.

* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu (who did the work) 
* Ann Loraine aloraine@uncc.edu (reviewer, Principal Investigator)

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT