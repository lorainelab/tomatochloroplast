# Tomato chloroplast sequence 

This module has the code to download the chloroplast sequence and save it as a fasta file. The only modificaiton made to the file is to use the sequence name that will be used in the annotation file so the annotation will match up to it.

The sequence is retrieved from GenBank/NCBI ([Solanum lycopersicum chloroplast, complete genome](http://www.ncbi.nlm.nih.gov/nuccore/NC_007898.3))

* * * 

## SaveFasta.Rmd

This markdown documents methods used download and save the sequence. 

* * * 

## results

This folder holds the output of the SaveFasta markdown. This file can be viewed in IGB.

* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu (who did the work) 
* Ann Loraine aloraine@uncc.edu (reviewer, Principal Investigator)
