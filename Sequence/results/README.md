# Tomato chloroplast - results

## What is this folder
In the module structure that the Loraine Lab uses for most projects, the "results" folder is a place to store the output files of a process.

* * *

### chrC-NC_007898.3.fasta
The sequence of the chloroplast in fasta format.


* * *
 
# Questions?

Contact:

* Ivory Blakley ieclabau@uncc.edu
* Ann Loraine aloraine@uncc.edu
